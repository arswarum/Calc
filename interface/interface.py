from tkinter import *

class Window():
    def __init__(self):
        # #super(Window, self).__init__(master)
        self.cal = Tk()
        self.cal.title('Calculator')

        self.operator = ""
        self.text_Input = StringVar()
        self.interface()
        self.cal.mainloop()

    def btnClick(self, number):
        #self.operator
        self.operator = self.operator + str(number)
        self.text_Input.set(self.operator)
    #     self.task = str(self.task) + str(number)
    #     self.UserIn.set(str(self.task))
    def btnClear(self):
        self.operator = ''
        self.text_Input.set('')

    def btnEqualsInput(self):
        sumup = str(eval(self.operator))
        self.text_Input.set(sumup)
        self.operator = str(sumup)




    def interface(self):
        """
        Create Window, Entry widget and Buttons for calculator
        """

        self.txtDisplay = Entry(self.cal, textvariable=self.text_Input, bd=20, insertwidth=4,
                           bg='powder blue', justify='right').grid(columnspan=4)

        self.btn7 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='7',
                      command=lambda: self.btnClick(7)).grid(row=1, column=0)
        self.btn8 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='8',
                      command=lambda: self.btnClick(8)).grid(row=1, column=1)
        self.btn9 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='9',
                      command=lambda: self.btnClick(9)).grid(row=1, column=2)

        self.addition = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='+',
                               command=lambda: self.btnClick('+')).grid(row=1, column=3)

        self.btn4 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='4',
                      command=lambda: self.btnClick(4)).grid(row=2, column=0)
        self.btn5 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='5',
                      command=lambda: self.btnClick(5)).grid(row=2, column=1)
        self.btn6 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='6',
                      command=lambda: self.btnClick(6)).grid(row=2, column=2)

        self.subtraction = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='-',
                                  command=lambda: self.btnClick('-')).grid(row=2, column=3)

        self.btn1 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='1',
                      command=lambda: self.btnClick(1)).grid(row=3, column=0)
        self.btn2 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='2',
                      command=lambda: self.btnClick(2)).grid(row=3, column=1)
        self.btn3 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='3',
                      command=lambda: self.btnClick(3)).grid(row=3, column=2)

        self.multiply = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='*',
                               command=lambda: self.btnClick('*')).grid(row=3, column=3)

        self.btn0 = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='0',
                      command=lambda: self.btnClick(0)).grid(row=4, column=0)
        self.btnClear = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='C', command=self.btnClear).grid(row=4, column=1)
        self.btnEquals = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='=', command = self.btnEqualsInput).grid(row=4, column=2)

        self.division = Button(self.cal, padx=16, pady=16, bd=8, fg='black', text='/',
                               command=lambda: self.btnClick('/')).grid(row=4, column=3)

